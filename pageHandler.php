<?php
// Project Name: Milestone1
// Project Version: 1.0
// Module Name: User Registration Module
// Module Version: 1.0
// Programmer Name: Justin Gewecke
// Date: 6/28/2020
// Description: This module is the registration page that a user visits in order to add their information to a database
// References: https://www.w3schools.com/php/php_mysql_insert.asp



// Connect
$link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$firstname = $_POST['FirstName'];
$lastname = $_POST['LastName'];
$email = $_POST['Email'];
$email2 = $_POST['ReenterEmail'];
$password = $_POST['Password'];

if ($email != $email2) {
    die("Email does not match! Please try again.");
}

// Attempt insert
$sql = "INSERT INTO users (FIRST_NAME, LAST_NAME, EMAIL, PASSWORD) VALUES ('$firstname', '$lastname', '$email', '$password')";
if(mysqli_query($link, $sql)){
    echo "Records inserted successfully.";
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}

// Close connection
mysqli_close($link);
?>